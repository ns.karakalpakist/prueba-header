import "./App.css";
import { useState } from "react";
import "bootstrap";
import { useForm } from "react-hook-form";
import Edad from "./edad";

function Form() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <form className="form-0" onSubmit={handleSubmit(onSubmit)}>
      <div className="primer-form">
        <div className="form-1">
          <div className="column-1">
            <h2>Nombre de institución*:</h2>
            <label htmlFor="nombre" />
            <input
              className="input-1"
              type="text"
              {...register("insti", {
                required: true,
              })}
              placeholder={"Escribe el nombre de la institución"}
            />
            {errors.insti?.type === "required" && (
              <p className="error">Este campo no puede estar vacío</p>
            )}
          </div>
          <div className="column-2">
            <h2>Cargo que ocupas*:</h2>
            <label htmlFor="cargo" />
            <input
              className="input-1"
              type="text"
              {...register("cargo", {
                required: true,
              })}
              placeholder={"Escribe tu cargo"}
            />
            {errors.cargo?.type === "required" && (
              <p className="error">Este campo no puede estar vacío</p>
            )}
          </div>
        </div>
        <div className="form-2">
          <div className="column-3">
            <h2>Tipo de institucion*:</h2>
            <label className="tipo-de-insti"></label>
            <select
              {...register("Tipo de institución")}
              className="input-1"
          
            >
              <option value="Colegio">Colegio</option>
              <option value="Biblioteca">Biblioteca</option>
              <option value="Fundación">Fundación</option>
              <option value="Empresa">Empresa</option>
              <option value="ONG">ONG</option>
            </select>
          </div>
          <div>
            <div className="column-4">
              <h2>Telefono*:</h2>
              <label />
              <input
                type="text"
                className="input-1"
                {...register("telefono", {
                  required: true,
                })}
                placeholder={"Escribe tu teléfono"}
              />
              {errors.telefono?.type === "required" && (
                <p className="error">Este campo no puede estar vacío</p>
              )}
            </div>
          </div>
        </div>
      </div>
      <div className="segundo-form">
        <div className="form-1">
          <div className="column-5">
            <h2>Nombre completo*:</h2>
            <label htmlFor="nombre" />
            <input
              className="input-1"
              type="text"
              {...register("nombre", {
                required: true,
              })}
              placeholder={"Escribe tu nombre y apellido aquí"}
            />
            {errors.nombre?.type === "required" && (
              <p className="error">Este campo no puede estar vacío</p>
            )}
          </div>
          <div className="column-6">
            <h2>Confirmar correo electrónico*:</h2>
            <label htmlFor="cargo" />
            <input
              className="input-1"
              type="text"
              {...register("email", {
                required: true,
                pattern: /\S+@\S+\.\S+/i,
              })}
              placeholder={"Escribe de nuevo tu correo electrónico"}
            />

            {errors.email?.type === "pattern" && <p>email no valido</p>}
          </div>
          <div className="column-7">
            <h2>Ciudad*</h2>
            <label htmlFor="ciudad" />
            <input
              className="input-1"
              type="text"
              {...register("cargo", {
                required: true,
              })}
              placeholder={"Escribe tu ciudad"}
            />
            {errors.ciudad?.type === "required" && (
              <p className="error">Este campo no puede estar vacío</p>
            )}
          </div>
        </div>

        <div className="form-2">
          <div className="column-5">
            <h2>Correo electrónico*:</h2>
            <label htmlFor="nombre" />
            <input
              className="input-1"
              type="text"
              {...register("email", {
                pattern: /\S+@\S+\.\S+/i,
              })}
              placeholder={"Escribe tu correo electrónico"}
            />
            {errors.email?.type === "pattern" && <p>email no valido</p>}
          </div>
          <div className="column-6">
            <h2>Edad*</h2>
            <label htmlFor="edad" />
            <input
              className="input-1"
              type="text"
              {...register("edad", {
                validate: Edad,
                required: true,
              })}
              placeholder={"Escribe tu edad"}
            />
            {errors.edad && <p className="error">No es valida</p>}
          </div>
          <div className="column-7">
            <h2>País*</h2>
            <label className="tipo-de-insti"/>
            <select {...register("Tipo de institución")} className="input-1">
              <option value="Colombia">Colombia</option>
              <option value="Venezuelaa">Venezuela</option>
              <option value="España">España</option>
              <option value="Argentina">Argentina</option>
              <option value="Mexico">Mexico</option>
            </select>
            {/* {errors.pais.type === "required" && (
              <p className="error">Este campo no puede estar vacío</p>
            )} */}
          </div>
        </div>
      </div>
      <input className="boton-enviar" type="submit" value="Enviar" />
    </form>
  );
}
export default Form;
