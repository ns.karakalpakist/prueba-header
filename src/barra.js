import "./App.css";
import { useState } from "react";
import "bootstrap";

function Barra() {
  /* const[Historia, setHistoria]=useState(true); */

  const handleChange = (data, newValue) => {
    console.log(data, newValue);

    let _materias = Materias;
    const filtered = _materias.filter((element) => data.id !== element.id);
    filtered.push({ ...data, checked: newValue });
    setMaterias(filtered);
  };
  const handleChange_nivel = (data, newValue) => {
    console.log(data, newValue);

    let _niveles = Niveles;
    const filtered2 = _niveles.filter((element) => data.id !== element.id);
    filtered2.push({ ...data, checked: newValue });
    setNiveles(filtered2);
  };

  const handleChange_genero = (data, newValue) => {
    console.log(data, newValue);

    let _generos = Generos;
    const filtered3 = _generos.filter((element) => data.id !== element.id);
    filtered3.push({ ...data, checked: newValue });
    setGeneros(filtered3);
  };

  const handleChange_edad = (data, newValue) => {
    console.log(data, newValue);

    let _edades = Edades;
    const filtered4 = _edades.filter((element) => data.id !== element.id);
    filtered4.push({ ...data, checked: newValue });
    setEdades(filtered4);
  };

  const handleChange_tiempo = (data, newValue) => {
    console.log(data, newValue);

    let _tiempos = Tiempos;
    const filtered5 = _tiempos.filter((element) => data.id !== element.id);
    filtered5.push({ ...data, checked: newValue });
    setTiempos(filtered5);
  };

  const enviarFiltros = () => {
    const filtros = {
      materias: [...Materias].filter((materia)=> materia.checked === true),
      niveles: [...Niveles].filter((niveles)=> niveles.checked === true),
      generos: [...Generos].filter((genero)=> genero.checked === true),
      edades: [...Edades].filter((edad)=> edad.checked === true),
      tiempos: [...Tiempos].filter((tiempo)=> tiempo.checked === true),

    };
    console.log(filtros);
  };

  const [open, setOpen] = useState(false);

  const [open2, setOpen2] = useState(false);

  const [open3, setOpen3] = useState(false);

  const [open4, setOpen4] = useState(false);

  const [open5, setOpen5] = useState(false);

  const [Materias, setMaterias] = useState([
    {
      id: "ciencias",
      name: "Ciencias",
      checked: false,
    },
    {
      id: "historia",
      name: "Historia y ensayo",
      checked: false,
    },
    {
      id: "letras",
      name: "Letras y números",
      checked: false,
    },
    {
      id: "artes",
      name: "Artes y manualidades",
      checked: false,
    },
    {
      id: "cancion",
      name: "Canción y poesía",
      checked: false,
    },
    {
      id: "narrativa",
      name: "Narrativa",
      checked: false,
    },
  ]);
  const [Niveles, setNiveles] = useState([
    {
      id: "primeros",
      name: "Primero lectores",
      checked: false,
    },
    {
      id: "intermedios",
      name: "Lectores intermedios",
      checked: false,
    },
    {
      id: "avanzados",
      name: "Lectores avanzados",
      checked: false,
    },
  ]);
  const [Generos, setGeneros] = useState([
    {
      id: "comic",
      name: "Cómic",
      checked: false,
    },
    {
      id: "libro",
      name: "Libro álbum",
      checked: false,
    },
    {
      id: "poesia",
      name: "Poesía",
      checked: false,
    },
    {
      id: "cro-en",
      name: "Crónica y ensayo",
      checked: false,
    },
    {
      id: "interactivo",
      name: "Interactivo",
      checked: false,
    },
    {
      id: "didactico",
      name: "Didáctico",
      checked: false,
    },
    {
      id: "teatro",
      name: "Teatro",
      checked: false,
    },
    {
      id: "cuento",
      name: "Cuento",
      checked: false,
    },
    {
      id: "novela",
      name: "Novela",
      checked: false,
    },
    {
      id: "informativo",
      name: "Informativo",
      checked: false,
    },
  ]);
  const [Edades, setEdades] = useState([
    {
      id: "3",
      name: "Desde 3 años",
      checked: false,
    },
    {
      id: "5",
      name: "Desde 5 años",
      checked: false,
    },
    {
      id: "7",
      name: "Desde 7 años",
      checked: false,
    },
    {
      id: "9",
      name: "Desde 9 años",
      checked: false,
    },
    {
      id: "11",
      name: "Desde 11 años",
      checked: false,
    },
    {
      id: "13",
      name: "Desde 13 años",
      checked: false,
    },
    {
      id: "15",
      name: "Desde 15 años",
      checked: false,
    },
  ]);
  const [Tiempos, setTiempos] = useState([
    {
      id: "5",
      name: "De 5 a 10 minutos",
      checked: false,
    },
    {
      id: "10",
      name: "De 10 a 20 minutos",
      checked: false,
    },
    {
      id: "20",
      name: "Más de 20 minutos",
      checked: false,
    },
  ]);

  /*  const datosRecopilados = document.getElementById("botonAplicar"); */

  /* datosRecopilados.addEventListener("click", () => {
      checkbox.forEach((e) => {
      if (e.checked == true) {
        console.log(e.name);
      }
    }); 
  });*/

  return (
    <div className="barra">
      <div className="buscar3">
        <input type="text" placeholder="Buscar" required />
        <div className="btn"></div>
        <i className="busqueda"></i>
      </div>
      <div className="flecha">
        <h1 className="opciones-1">
          Materia {Materias.filter((materia) => materia.checked == true).length}
        </h1>
        <svg
          className="flecha-1"
          onClick={() => setOpen(!open)}
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
        </svg>
      </div>
      {open && (
        <div className="dropdown-1">
          <ul>
            {Materias.map((materia) => (
              <li className="materias" key={materia?.id}>
                <input
                  type="checkbox"
                  checked={materia?.checked}
                  onChange={(e) => handleChange(materia, e.target.checked)}
                />
                {materia?.name}
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="flecha-2">
        <h1 className="opciones-2">
          Nivel de lectura{" "}
          {Niveles.filter((nivel) => nivel.checked == true).length}
        </h1>
        <svg
          className="flecha-2"
          onClick={() => setOpen2(!open2)}
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
        </svg>
      </div>
      {open2 && (
        <div className="dropdown-2">
          <ul>
            {Niveles.map((nivel) => (
              <li className="nivel" key={nivel?.id}>
                <input
                  type="checkbox"
                  checked={nivel?.checked}
                  onChange={(e) => handleChange_nivel(nivel, e.target.checked)}
                />
                {nivel?.name}
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="flecha-3">
        <h1 className="opciones-2">
          Género {Generos.filter((genero) => genero.checked == true).length}
        </h1>
        <svg
          className="flecha-2"
          onClick={() => setOpen3(!open3)}
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
        </svg>
      </div>
      {open3 && (
        <div className="dropdown-2">
          <ul>
            {Generos.map((genero) => (
              <li className="genero" key={genero?.id}>
                <input
                  className="generos2"
                  type="checkbox"
                  checked={genero?.checked}
                  onChange={(e) =>
                    handleChange_genero(genero, e.target.checked)
                  }
                />
                {genero?.name}
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="flecha-4">
        <h1 className="opciones-2">
          Edad {Edades.filter((edad) => edad.checked == true).length}
        </h1>
        <svg
          className="flecha-2"
          onClick={() => setOpen4(!open4)}
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
        </svg>
      </div>
      {open4 && (
        <div className="dropdown-2">
          <ul>
            {Edades.map((edad) => (
              <li className="edad" key={edad?.id}>
                <input
                  type="checkbox"
                  checked={edad?.checked}
                  onChange={(e) => handleChange_edad(edad, e.target.checked)}
                />
                {edad?.name}
              </li>
            ))}
          </ul>
        </div>
      )}
      <div className="flecha-5">
        <h1 className="opciones-2">
          Tiempo de lectura{" "}
          {Tiempos.filter((tiempo) => tiempo.checked == true).length}
        </h1>
        <svg
          className="flecha-2"
          onClick={() => setOpen5(!open5)}
          xmlns="http://www.w3.org/2000/svg"
          width="16"
          height="16"
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
        </svg>
      </div>
      {open5 && (
        <div className="dropdown-2">
          <ul>
            {Tiempos.map((tiempo) => (
              <li className="tiempo" key={tiempo?.id}>
                <input
                  type="checkbox"
                  checked={tiempo?.checked}
                  onChange={(e) =>
                    handleChange_tiempo(tiempo, e.target.checked)
                  }
                />
                {tiempo?.name}
              </li>
            ))}
          </ul>
        </div>
      )}
      <button
        id="botonAplicar"
        className="aplicar"
        onClick={()=>enviarFiltros()}
      >Aplica filtros</button>
      
    </div>
  );
}

export default Barra;
