import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import Barra from "./barra";
import "bootstrap";
import Form from "./form";
/* import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "fortawesome/free-solid-svg-icons"; */

function App() {
  const [menu, setMenu] = useState(false);

  const abrirCerrar = () => {
    setMenu(!menu);
    console.log("abrir /Cerrar");
  };
  return (
    <div className="App">
      <header className="App-header">
        <button className="boton" onClick={abrirCerrar}>
          <svg
            className="menuf"
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            viewBox="0 0 16 16"
          >
            <path
              fill-rule="evenodd"
              d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
            />
          </svg>{" "}
          <h1>Explorar</h1>
        </button>

        <div className="buscar">
          <input
            type="text"
            placeholder="Búsqueda por título, autor, editorial"
            required
          />
          <div className="btn"></div>
          <i className="busqueda"></i>
        </div>

        <img className="logo2" src="MM.png"></img>

        <ul className="nav-derecha">
          <a className="que-es" href="index.js">
            {" "}
            ¿Qué es?
          </a>

          <a className="quiero-make" href="index.js">
            {" "}
            Quiero MakeMake
          </a>

          <a className="blog" href="index.js">
            {" "}
            Blog
          </a>

          <a className="iniciar-sesion" href="index.js">
            {" "}
            Inciar sesión
          </a>
        </ul>
      </header>

      <nav className={`menu-cuadro ${menu ? "isActive" : ""}`}>
        <div className="cuadro-1">
          <div className={`buscar2 ${menu ? "activo" : ""}`}>
            <input
              type="text"
              placeholder="Búsqueda por título, autor, editorial"
              required
            />
            <div className="btn"></div>
            <i className="busqueda"></i>
          </div>
        </div>
      </nav>
      <div className="wrapper">
  
          {" "}
          {/* <Barra></Barra> */}
          <Form></Form>
       
        
      </div>
    </div>
  );
}

export default App;
