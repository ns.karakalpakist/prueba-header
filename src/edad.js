import "./App.css";
import { useState } from "react";
import "bootstrap";
import { useForm } from "react-hook-form";

const Edad = (value) => {
  return value >= 18 && value <=100;
};
export default { Edad };
